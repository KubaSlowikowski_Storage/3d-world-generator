import * as THREE from 'three';

let scene, camera, renderer;

scene = new THREE.Scene();
camera = new THREE.PerspectiveCamera(40, window.innerWidth / window.innerHeight, 0.1, 10000);
camera.position.set(0, 0, 50);

renderer = new THREE.WebGLRenderer({antialias: true, alpha: true});
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);

function handleResize() {
  const {innerHeight, innerWidth} = window;
  renderer.setSize(innerWidth, innerHeight);
  camera.aspect = innerWidth / innerHeight;
  camera.updateProjectionMatrix();
}

function createSphere(radius = 1, color = 0xffffff) {
  const sphereMaterial = new THREE.MeshPhongMaterial({
    color: color,
    shininess: 30,
  })
  const sphereGeometry = new THREE.SphereGeometry(radius, 20, 20)
  return new THREE.Mesh(sphereGeometry, sphereMaterial);
}

function createElectron(radius = 0.5, color = 0xffffff) {
  const sphere = createSphere(radius, color);
  const pivot = new THREE.Object3D();
  pivot.add(sphere);
  return {
    sphere,
    pivot
  }
}

function createPointLight(intensity = 1, color = 0xffffff) {
  return new THREE.PointLight(color, intensity);
}

const nucleus = createSphere(3);
const light1 = createPointLight(0.9);
const light2 = createPointLight(0.4);
light1.position.set(120, 20, 60);
light2.position.set(-60, 0, 20);

scene.add(nucleus, light1, light2);

//electrons
const e1 = createElectron(0.5);
const e2 = createElectron(0.5);
const e3 = createElectron(0.5);
const e4 = createElectron(0.5);
e1.sphere.position.set(10, 0, 0);
e2.sphere.position.set(-10, 0, 0);
e3.sphere.position.set(0, 10, 0);
e4.sphere.position.set(0, -10, 0);
nucleus.add(e1.pivot, e2.pivot, e3.pivot, e4.pivot);

scene.add(e1, e2, e3, e4);

function loop() {
  renderer.render(scene, camera);

  e1.pivot.rotation.z += 0.02;
  e2.pivot.rotation.z += 0.02;
  e3.pivot.rotation.z += 0.02;
  e4.pivot.rotation.z += 0.02;
  nucleus.rotation.x += 0.01;
  nucleus.rotation.y += 0.02;
  nucleus.rotation.z += 0.03;

  requestAnimationFrame(loop);
}

loop();
window.addEventListener('resize', handleResize);
