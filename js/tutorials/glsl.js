import * as THREE from 'three';
import {OrbitControls} from 'three/examples/jsm/controls/OrbitControls';


const renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
// renderer.shadowMap.enabled = true;
document.body.appendChild(renderer.domElement);

// Scene
const scene = new THREE.Scene();

// Camera
const camera = new THREE.PerspectiveCamera(
  45,
  window.innerWidth / window.innerHeight,
  0.1,
  1000
);

const orbit = new OrbitControls(camera, renderer.domElement);
const axesHelper = new THREE.AxesHelper(5);
scene.add(axesHelper);
camera.position.set(-25, 25, 25);
orbit.update();

// ambient light which is for the whole scene
const ambientLight = new THREE.AmbientLight(0xffffff, 0.5);
ambientLight.castShadow = true;
scene.add(ambientLight);

// directional light - parallel sun rays
const directionalLight = new THREE.DirectionalLight(0xffffff, 1);
// this.directionalLight.castShadow = true;
directionalLight.position.set(0, 32, 64);
scene.add(directionalLight);

const clock = new THREE.Clock();

const uniformData = {
  u_time: {
    type: 'f',
    value: clock.getElapsedTime()
  }
};

function animate(time) {
  renderer.render(scene, camera);
  uniformData.u_time.value += clock.getDelta();
}

renderer.setAnimationLoop(animate);

const boxGeometry = new THREE.BoxGeometry(24, 4, 24, 24, 4, 24);
// const boxMaterial = new THREE.MeshStandardMaterial({
//   color: 'red',
//   wireframe: true
// });
const boxMaterial = new THREE.ShaderMaterial({
  wireframe: true,
  uniforms: uniformData,
  // language=Glsl
  vertexShader: `
      varying vec3 pos;
      uniform float u_time;

      void main() {
          // projectionMatrix, modelViewMatrix, position -> passed in from Three.js
          vec4 result;
          pos = position;

          //    result = vec4(position.x, sin(position.z) + position.y, position.z, 1.0);
          //    result = vec4(position.x, sin(position.z) + position.y, position.z, 1.0);
          //    result = vec4(position.x, position.y + sin(u_time), position.z, 1.0);
          result = vec4(position.x, 0.5 * sin(position.z + u_time) + sin(position.x / 2.0 + u_time) + position.y, position.z, 1.0);
          //    result = vec4(position.x, 4.0 * sin(position.z / 4.0 + u_time) + position.y, position.z, 1.0);

          gl_Position = projectionMatrix
          * modelViewMatrix
          * result;
      }`,
  fragmentShader: `
      varying vec3 pos;
      uniform float u_time;

      void main() {
          if (pos.x >= 0.0) {
              gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
          } else {
              // gl_FragColor = vec4(0.0, 1.0, 0.0, 1.0);
              gl_FragColor = vec4(abs(sin(u_time)), abs(cos(u_time)), 0.0, 1.0);
          }
          //    gl_FragColor = vec4(abs(sin(u_time)), abs(cos(u_time)), 0.0, 1.0);
}`,

});
const box = new THREE.Mesh(boxGeometry, boxMaterial);
scene.add(box);

