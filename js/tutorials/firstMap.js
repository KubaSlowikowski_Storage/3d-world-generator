import * as THREE from 'three';
import {OrbitControls} from 'three/examples/jsm/controls/OrbitControls';
import * as dat from 'dat.gui';
import noiseImage from '../img/noise.png';
// import slawekImage from '../img/slawek.png';
// import slawekColorImage from '../img/slawekColor.png';
// import julaColorImage from '../img/jula.png';
// import julaBW from '../img/julaBW.png';
import mountainsImage from '../img/Mountains.jpg';
// import sunImage from '../img/sun.png';

const renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
// renderer.shadowMap.enabled = true;
document.body.appendChild(renderer.domElement);

// Texture loader
const textureLoader = new THREE.TextureLoader();
const noise = textureLoader.load(noiseImage);
// const slawek = textureLoader.load(slawekImage);
// const slawekColor = textureLoader.load(slawekColorImage);
// const jula = textureLoader.load(julaBW);
// const julaColor = textureLoader.load(julaColorImage);
const mountains = textureLoader.load(mountainsImage);
// const sun = textureLoader.load(sunImage);
// sun.repeat.set(8,8);
// sun.wrapT = true;
// sun.wrapS = true;

// Debug
const gui = new dat.GUI();

// Scene
const scene = new THREE.Scene();

// Camera
const camera = new THREE.PerspectiveCamera(
  45,
  window.innerWidth / window.innerHeight,
  0.1,
  1000
);

const orbit = new OrbitControls(camera, renderer.domElement);

// Lights
const spotlight = new THREE.SpotLight(0xFFFFFF);
scene.add(spotlight);
spotlight.position.set(-250, 100, 0);
// spotlight.castShadow = true;
spotlight.angle = 0.5;
spotlight.intensity = 3;

const pointLight = new THREE.PointLight('rgb(255,241,229)');
scene.add(pointLight);
pointLight.position.set(0,100,0);
pointLight.intensity = 0.5;

const pointLight2 = new THREE.PointLight('rgb(255,241,229)');
scene.add(pointLight2);
pointLight2.position.set(0,-200,100);
pointLight2.intensity = 1.5;

// Helpers
const axesHelper = new THREE.AxesHelper(5);
scene.add(axesHelper);
camera.position.set(-40, 200, 200);
orbit.update();

// const sLightHelper = new THREE.SpotLightHelper(spotlight);
// scene.add(sLightHelper);

// const gridHelper = new THREE.GridHelper(30);
// scene.add(gridHelper);

// Objects
const planeGeometry = new THREE.PlaneBufferGeometry(200, 200, 200, 200);
const sphereGeometry = new THREE.SphereGeometry(50, 200, 200);
const planeMaterial = new THREE.MeshLambertMaterial({
  color: 'gray',
  // wireframe: true,
  // map: slawekColor,
  map: mountains,
  // map: julaColor,
  displacementMap: noise,
  // displacementMap: slawek,
  // displacementMap: jula,
  displacementScale: 5,
  scale: 5,
  side: THREE.DoubleSide,
  metalness: 1.0
});
const plane = new THREE.Mesh(sphereGeometry, planeMaterial);
scene.add(plane);

// plane.receiveShadow = true;
// plane.castShadow = true;
plane.rotation.x = -Math.PI * .5;

const options = {
  rotationX: 0.005,
  rotationZ: 0.005
}

gui.add(spotlight, "angle").min(0).max(1);
gui.add(spotlight, "penumbra").min(0).max(1);
gui.add(spotlight, "intensity").min(0).max(5);
gui.add(spotlight.position, "x").min(-500).max(500);
gui.add(spotlight.position, "y").min(-500).max(500);
gui.add(spotlight.position, "z").min(-500).max(500);
gui.add(options, "rotationX").min(0).max(0.1);
gui.add(options, "rotationZ").min(0).max(0.1);

function animate(time) {
  renderer.render(scene, camera);

  plane.rotation.z += options.rotationZ
  plane.rotation.x += options.rotationX;
}

renderer.setAnimationLoop(animate);
