import {GuiContext} from "./src/helpers/guiContext";
import {initialize} from "./src/init";
import {
  continentalnessFbmParams,
  erosionFbmParams,
  peaks_and_valleys_FbmParams,
  precipitationFbmParams,
  temperatureFbmParams
} from "./src/noise/noise";

import {ADD_CHUNKS, CHUNK_SIZE} from "./src/configuration/configuration";
import {CONTINENTALNESS_RATIO} from "./src/heatmaps/continentalness";
import {EROSION_RATIO} from "./src/heatmaps/erosion";
import {PV_RATIO} from "./src/heatmaps/peaksAndValleys";

const { scene, renderer, camera, stats, terrainGenerator } = initialize(animate);

// Objects
terrainGenerator.createChunk(0, 0);
terrainGenerator.createChunk(1, 0);
terrainGenerator.createChunk(-1, 0);
terrainGenerator.createChunk(0, 1);
terrainGenerator.createChunk(0, -1);
terrainGenerator.createChunk(1, 1);
terrainGenerator.createChunk(1, -1);
terrainGenerator.createChunk(-1, 1);
terrainGenerator.createChunk(-1, -1);

// Debug
const gui = new GuiContext();
addTerrainGui();
addContinentalnessGui();
addErosionGui();
addPV_Gui();
addTemperatureGui();
addPrecipitationGui();

function animate(time) {
  renderer.render(scene, camera);
  stats.update();
  if (ADD_CHUNKS) {
    const chunks_away_x = Math.round(camera.position.x / CHUNK_SIZE);
    const chunks_away_y = Math.round(camera.position.z / CHUNK_SIZE);

    if (terrainGenerator.chunks.findIndex(chunk => chunk.x === chunks_away_x && chunk.y === chunks_away_y) === -1) {
      terrainGenerator.createChunk(chunks_away_x, chunks_away_y);
    }
  }
}

function addContinentalnessGui() {
  const folder = gui.addFolder("Continentalness heatmap");
  folder.add({ contWireframe: true }, "contWireframe").onChange(e => terrainGenerator.chunks.forEach(c => c.chunk.continentalnessChunk.material.wireframe = e));
  folder.add({ visible: false }, "visible").onChange(visible => {
    terrainGenerator.chunks.forEach(bean => {
      bean.chunk.continentalnessChunk.material.visible = visible;
    });
  });
  folder.add(continentalnessFbmParams, "octaves").min(1).max(16).onChange(_ => {
    terrainGenerator.rebuildContinentalnessChunks();
    terrainGenerator.rebuildTerrainChunks();
  });
  folder.add(continentalnessFbmParams, "frequency").min(0.0001).max(0.3).onChange(_ => {
    terrainGenerator.rebuildContinentalnessChunks();
    terrainGenerator.rebuildTerrainChunks();
  });
  folder.add(continentalnessFbmParams, "lacunarity").min(0.1).max(2).onChange(_ => {
    terrainGenerator.rebuildContinentalnessChunks();
    terrainGenerator.rebuildTerrainChunks();
  });
  folder.add(continentalnessFbmParams, "persistence").min(0.1).max(2).onChange(_ => {
    terrainGenerator.rebuildContinentalnessChunks();
    terrainGenerator.rebuildTerrainChunks();
  });
  folder.add(CONTINENTALNESS_RATIO, "value").min(0).max(50).onChange(_ => {
    terrainGenerator.rebuildContinentalnessChunks();
    terrainGenerator.rebuildTerrainChunks();
  }).name("height");

  // folder.open();
}

function addErosionGui() {
  const folder = gui.addFolder("Erosion heatmap");
  folder.add({ contWireframe: true }, "contWireframe").onChange(e => terrainGenerator.chunks.forEach(c => c.chunk.erosionChunk.material.wireframe = e));
  folder.add({ visible: false }, "visible").onChange(visible => {
    terrainGenerator.chunks.forEach(bean => {
      bean.chunk.erosionChunk.material.visible = visible;
    });
  });
  folder.add(erosionFbmParams, "octaves").min(1).max(16).onChange(_ => {
    terrainGenerator.rebuildErosionChunks();
    terrainGenerator.rebuildTerrainChunks();
  });
  folder.add(erosionFbmParams, "frequency").min(0.0001).max(0.3).onChange(_ => {
    terrainGenerator.rebuildErosionChunks();
    terrainGenerator.rebuildTerrainChunks();
  });
  folder.add(erosionFbmParams, "lacunarity").min(0.1).max(2).onChange(_ => {
    terrainGenerator.rebuildErosionChunks();
    terrainGenerator.rebuildTerrainChunks();
  });
  folder.add(erosionFbmParams, "persistence").min(0.1).max(2).onChange(_ => {
    terrainGenerator.rebuildErosionChunks();
    terrainGenerator.rebuildTerrainChunks();
  });
  folder.add(EROSION_RATIO, "value").min(0).max(50).onChange(_ => {
    terrainGenerator.rebuildErosionChunks();
    terrainGenerator.rebuildTerrainChunks();
  }).name("height");

  // folder.open();
}

function addPV_Gui() {
  const folder = gui.addFolder("Peaks and Valleys heatmap");
  folder.add({ contWireframe: true }, "contWireframe").onChange(e => terrainGenerator.chunks.forEach(c => c.chunk.pvChunk.material.wireframe = e));
  folder.add({ visible: false }, "visible").onChange(visible => {
    terrainGenerator.chunks.forEach(bean => {
      bean.chunk.pvChunk.material.visible = visible;
    });
  });
  folder.add(peaks_and_valleys_FbmParams, "octaves").min(1).max(16).onChange(_ => {
    terrainGenerator.rebuildPV_Chunks();
    terrainGenerator.rebuildTerrainChunks();
  });
  folder.add(peaks_and_valleys_FbmParams, "frequency").min(0.0001).max(0.3).onChange(_ => {
    terrainGenerator.rebuildPV_Chunks();
    terrainGenerator.rebuildTerrainChunks();
  });
  folder.add(peaks_and_valleys_FbmParams, "lacunarity").min(0.1).max(2).onChange(_ => {
    terrainGenerator.rebuildPV_Chunks();
    terrainGenerator.rebuildTerrainChunks();
  });
  folder.add(peaks_and_valleys_FbmParams, "persistence").min(0.1).max(2).onChange(_ => {
    terrainGenerator.rebuildPV_Chunks();
    terrainGenerator.rebuildTerrainChunks();
  });
  folder.add(PV_RATIO, "value").min(0).max(50).onChange(_ => {
    terrainGenerator.rebuildPV_Chunks();
    terrainGenerator.rebuildTerrainChunks();
  }).name("height");


  // folder.open();
}

function addTerrainGui() {
  const folder = gui.addFolder("Terrain");
  folder.add({ contWireframe: false }, "contWireframe").onChange(e => terrainGenerator.chunks.forEach(c => c.chunk.terrainChunk.material.wireframe = e));
  folder.add({ visible: true }, "visible").onChange(visible => {
    terrainGenerator.chunks.forEach(bean => {
      bean.chunk.terrainChunk.material.visible = visible;
    });
  });

  folder.open();
}

function addTemperatureGui() {
  const folder = gui.addFolder("Temperature heatmap");
  folder.add({ contWireframe: true }, "contWireframe").onChange(e => terrainGenerator.chunks.forEach(c => c.chunk.temperatureChunk.material.wireframe = e));
  folder.add({ visible: false }, "visible").onChange(visible => {
    terrainGenerator.chunks.forEach(bean => {
      bean.chunk.temperatureChunk.material.visible = visible;
    });
  });
  folder.add(temperatureFbmParams, "octaves").min(1).max(16).onChange(_ => {
    terrainGenerator.rebuildTemperatureChunks();
    terrainGenerator.rebuildTerrainChunks();
  });
  folder.add(temperatureFbmParams, "frequency").min(0.0001).max(0.3).onChange(_ => {
    terrainGenerator.rebuildTemperatureChunks();
    terrainGenerator.rebuildTerrainChunks();
  });
  folder.add(temperatureFbmParams, "lacunarity").min(0.1).max(2).onChange(_ => {
    terrainGenerator.rebuildTemperatureChunks();
    terrainGenerator.rebuildTerrainChunks();
  });
  folder.add(temperatureFbmParams, "persistence").min(0.1).max(2).onChange(_ => {
    terrainGenerator.rebuildTemperatureChunks();
    terrainGenerator.rebuildTerrainChunks();
  }).name("height");

  // folder.open();
}

function addPrecipitationGui() {
  const folder = gui.addFolder("Precipitation heatmap");
  folder.add({ contWireframe: true }, "contWireframe").onChange(e => terrainGenerator.chunks.forEach(c => c.chunk.precipitationChunk.material.wireframe = e));
  folder.add({ visible: false }, "visible").onChange(visible => {
    terrainGenerator.chunks.forEach(bean => {
      bean.chunk.precipitationChunk.material.visible = visible;
    });
  });
  folder.add(precipitationFbmParams, "octaves").min(1).max(16).onChange(_ => {
    terrainGenerator.rebuildPrecipitationChunks();
    terrainGenerator.rebuildTerrainChunks();
  });
  folder.add(precipitationFbmParams, "frequency").min(0.0001).max(0.3).onChange(_ => {
    terrainGenerator.rebuildPrecipitationChunks();
    terrainGenerator.rebuildTerrainChunks();
  });
  folder.add(precipitationFbmParams, "lacunarity").min(0.1).max(2).onChange(_ => {
    terrainGenerator.rebuildPrecipitationChunks();
    terrainGenerator.rebuildTerrainChunks();
  });
  folder.add(precipitationFbmParams, "persistence").min(0.1).max(2).onChange(_ => {
    terrainGenerator.rebuildPrecipitationChunks();
    terrainGenerator.rebuildTerrainChunks();
  });

  // folder.open();
}
