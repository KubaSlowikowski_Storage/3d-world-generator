import alea from "alea";
import {createNoise2D} from "simplex-noise";

const SCALE = 0.5;

export function getPerlinNoise(seed) {
  // create a new random function based on the seed
  const prng = alea(seed);
  // use the seeded random function to initialize the noise function
  return createNoise2D(prng);
}

export const continentalnessFbmParams = {
  octaves: 8, // how many iteration of simplex noise should we layer together
  frequency: 0.008 * SCALE, // how much detail should we show per unit of space (kinda zoom level)
  lacunarity: 2.0, // how quickly should our frequency grow per octave
  persistence: 0.5, // How quickly should our amplitude shrink per octave
}

export const erosionFbmParams = {
  octaves: 8, // how many iteration of simplex noise should we layer together
  frequency: 0.007 * SCALE, // how much detail should we show per unit of space (kinda zoom level)
  lacunarity: 2.0, // how quickly should our frequency grow per octave
  persistence: 0.5, // How quickly should our amplitude shrink per octave
}

export const peaks_and_valleys_FbmParams = {
  octaves: 8, // how many iteration of simplex noise should we layer together
  frequency: 0.025 * SCALE, // how much detail should we show per unit of space (kinda zoom level)
  lacunarity: 2.0, // how quickly should our frequency grow per octave
  persistence: 0.5, // How quickly should our amplitude shrink per octave
}

export const temperatureFbmParams = {
  octaves: 8, // how many iteration of simplex noise should we layer together
  frequency: 0.004 * SCALE * 2, // how much detail should we show per unit of space (kinda zoom level)
  lacunarity: 2.0, // how quickly should our frequency grow per octave
  persistence: 0.5, // How quickly should our amplitude shrink per octave
}

export const precipitationFbmParams = {
  octaves: 8, // how many iteration of simplex noise should we layer together
  frequency: 0.016 * SCALE, // how much detail should we show per unit of space (kinda zoom level)
  lacunarity: 2.0, // how quickly should our frequency grow per octave
  persistence: 0.5, // How quickly should our amplitude shrink per octave
}
