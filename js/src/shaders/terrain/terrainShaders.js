export const terrainVertexShader = `
    uniform vec3 meshPosition;
    varying vec2 vUv;
    varying vec3 pos;
    uniform sampler2D riversMap;
    varying vec3 globalPosition;

    void main() {
        float river = texture2D(riversMap, uv).x;

        globalPosition = meshPosition + position;
        pos = position;
        vUv = uv;

        if (river == 1.0 / 255.0) {
            vec3 newPosition = position;
            newPosition.z = newPosition.z - 0.05;
            gl_Position = projectionMatrix * modelViewMatrix * vec4(newPosition, 1.0);
        } else {
            gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
        }
    }
`;

// language=Glsl
export const terrainFragmentShader = `
    varying vec2 vUv;
    varying vec3 pos;
    uniform sampler2D climateTexture;
    uniform sampler2D riversMap;

    uniform sampler2D boreal_forest_texture;
    uniform sampler2D savanna_texture;
    uniform sampler2D subtropical_dessert_texture;
    uniform sampler2D temperate_grassland_cold_dessert_texture;
    uniform sampler2D temperate_seasonal_forest_texture;
    uniform sampler2D temperate_rainforest_texture;
    uniform sampler2D tropical_rainforest_texture;
    uniform sampler2D tundra_rainforest_texture;
    uniform sampler2D woodland_shrubland_texture;
    uniform sampler2D river_texture;
    uniform sampler2D water_texture;
    uniform sampler2D snow_texture;
    uniform sampler2D rocks_texture;

    uniform bool fogEnabled;
    uniform float fogNear;
    uniform float fogFar;

    uniform float waterLevel;
    uniform int chunk_size;

    void main() {
        float scale = 40.0;
        vec2 scaledUV = fract(vec2(vUv.x * scale, vUv.y * scale));
        vec2 offsetUV = fract(scaledUV);

        float f_chunk_size = float(chunk_size);
        if (pos.x == f_chunk_size / 2.0 || pos.y == f_chunk_size / 2.0) {
            gl_FragColor = vec4(1.0, 0.0, 0.0, 0.0);
            return;
        }


        float biome = texture2D(climateTexture, vUv).x;
        float river = texture2D(riversMap, vUv).x;

        if (river == 1.0 / 255.0) {
            gl_FragColor = texture2D(river_texture, offsetUV);
        } else if (biome == 1.0 / 255.0) {
            gl_FragColor = texture2D(tundra_rainforest_texture, offsetUV);
        } else if (biome == 2.0 / 255.0) {
            gl_FragColor = texture2D(boreal_forest_texture, offsetUV);
        } else if (biome == 3.0 / 255.0) {
            gl_FragColor = texture2D(temperate_grassland_cold_dessert_texture, offsetUV);
        } else if (biome == 4.0 / 255.0) {
            gl_FragColor = texture2D(woodland_shrubland_texture, offsetUV);
        } else if (biome == 5.0 / 255.0) {
            gl_FragColor = texture2D(subtropical_dessert_texture, offsetUV);
        } else if (biome == 6.0 / 255.0) {
            gl_FragColor = texture2D(temperate_seasonal_forest_texture, offsetUV);
        } else if (biome == 7.0 / 255.0) {
            gl_FragColor = texture2D(savanna_texture, offsetUV);
        } else if (biome == 8.0 / 255.0) {
            gl_FragColor = texture2D(temperate_rainforest_texture, offsetUV);
        } else if (biome == 9.0 / 255.0) {
            gl_FragColor = texture2D(tropical_rainforest_texture, offsetUV);
        }/** else if (biome == 10.0 / 255.0) {
            gl_FragColor = texture2D(river_texture, offsetUV);
        }*/ else {
            gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
        }

        if (pos.z < waterLevel) {
            gl_FragColor = texture2D(water_texture, offsetUV);
        }

        if (pos.z <= 10.5 && pos.z > 10.0) {
            float scale2 = 80.0;
            vec2 scaledUV2 = fract(vec2(vUv.x * scale2, vUv.y * scale2));
            vec2 offsetUV2 = fract(scaledUV2);
            gl_FragColor = texture2D(rocks_texture, offsetUV2);
        }
        if (pos.z > 10.5) {
            gl_FragColor = texture2D(snow_texture, offsetUV);
        }

        if (fogEnabled) {
            float depth = gl_FragCoord.z / gl_FragCoord.w;
            float fogFactor = smoothstep( fogNear, fogFar, depth );
            gl_FragColor.rgb = mix( gl_FragColor.rgb, vec3(185.0 / 255.0, 222.0 / 255.0, 254.0 / 255.0), fogFactor );
        }
    }`;
