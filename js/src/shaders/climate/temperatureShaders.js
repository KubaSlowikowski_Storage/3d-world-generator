export const temperatureFragmentShader = `varying vec3 pos;

void main() {
    float borderWidth = 0.15;
    float height = 1.0;

    vec3 purple = (smoothstep(0.0 - borderWidth, height/5.0, pos.z) - smoothstep(height/5.0 - borderWidth, height/5.0, pos.z)) * vec3(68.0/255.0, 0.0, 92.0/255.0);
    vec3 blue = (smoothstep(height/5.0 - borderWidth, 2.0*height/5.0, pos.z) - smoothstep(2.0*height/5.0 - borderWidth, 2.0*height/5.0, pos.z)) * vec3(0.0,193.0/255.0,1.0);
    vec3 green = (smoothstep(2.0*height/5.0 - borderWidth, 3.0*height/5.0, pos.z) - smoothstep(3.0*height/5.0 - borderWidth, 3.0*height/5.0, pos.z)) * vec3(0.0,1.0,0.0);
    vec3 yellow = (smoothstep(3.0*height/5.0 - borderWidth, 4.0*height/5.0, pos.z) - smoothstep(4.0*height/5.0 - borderWidth, 4.0*height/5.0, pos.z)) * vec3(1.0,1.0,0.0);
    vec3 red = smoothstep(4.0*height/5.0 - borderWidth, height, pos.z) * vec3(1.0,0.0,0.0);

    gl_FragColor = vec4(purple + blue + green + yellow + red, 1.0);
}`;
