import * as THREE from "three";
import {AXES_HELPER, FOG, SKY_ENABLED} from "../configuration/configuration";
import Stats from 'three/examples/jsm/libs/stats.module'
import {sky} from "./sky";

export class SceneContext {

  constructor() {
    const scene = new THREE.Scene();
    this.scene = scene;

    if (SKY_ENABLED) {
      scene.background = sky;
    } else {
      scene.background = new THREE.Color('black');
    }

    if (FOG.enabled) {
      scene.fog = new THREE.Fog(`rgb(209, 243, 255)`, FOG.near, FOG.far);
    }

    this.stats = new Stats();

    if (AXES_HELPER) {
      const axesHelper = new THREE.AxesHelper(10); //The X axis is red. The Y axis is green. The Z axis is blue.
      scene.add(axesHelper);
    }
  }

  addLights() {
    // ambient light which is for the whole scene
    const ambientLight = new THREE.AmbientLight(0xffffff, 0.5);
    this.scene.add(ambientLight);
  }
}
