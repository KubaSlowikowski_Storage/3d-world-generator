import * as dat from 'dat.gui';

export class GuiContext {

  constructor() {
    const gui = new dat.GUI();
    this.gui = gui;
  }

  addOption(object, property) {
    return this.gui.add(object, property);
  }

  addFolder(name) {
    return this.gui.addFolder(name);
  }
}
