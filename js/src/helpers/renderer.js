import * as THREE from "three";
import {ANTIALIAS_ENABLED, SHADOWS_ENABLED} from "../configuration/configuration";

export function initializeRenderer(animationLoop) {
  const renderer = new THREE.WebGLRenderer({
    antialias: ANTIALIAS_ENABLED,
  });
  renderer.setSize(window.innerWidth, window.innerHeight);
  renderer.setAnimationLoop(animationLoop);
  renderer.shadowMap.enabled = SHADOWS_ENABLED;
  document.body.appendChild(renderer.domElement);

  return renderer;
}
