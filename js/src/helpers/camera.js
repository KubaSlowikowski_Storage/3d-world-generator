import * as THREE from "three";
import {CAMERA_CONFIG} from "../configuration/configuration";
import {OrbitControls} from 'three/examples/jsm/controls/OrbitControls';

export class CameraContext {
  constructor(renderer) {
    const camera = new THREE.PerspectiveCamera(
      CAMERA_CONFIG.fov,
      window.innerWidth / window.innerHeight,
      CAMERA_CONFIG.near,
      CAMERA_CONFIG.far
    );
    this.camera = camera;
    this.renderer = renderer;

    const orbit = new OrbitControls(camera, renderer.domElement);
    camera.position.set(CAMERA_CONFIG.position_x, CAMERA_CONFIG.position_y, CAMERA_CONFIG.position_z);
    orbit.update();

    this.addOnResize();
  }

  addOnResize() {
    window.addEventListener('resize', function () {
      this.camera.aspect = window.innerWidth / window.innerHeight;
      this.camera.updateProjectionMatrix();
      this.renderer.setSize(window.innerWidth, window.innerHeight);
    });
  }
}
