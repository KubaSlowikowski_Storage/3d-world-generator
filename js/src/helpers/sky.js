import * as THREE from "three";
import back from '../../../img/sky/Daylight Box_Back.bmp';
import bottom from '../../../img/sky/Daylight Box_Bottom.bmp';
import front from '../../../img/sky/Daylight Box_Front.bmp';
import left from '../../../img/sky/Daylight Box_Left.bmp';
import right from '../../../img/sky/Daylight Box_Right.bmp';
import top from '../../../img/sky/Daylight Box_Top.bmp';

export const sky = new THREE.CubeTextureLoader().load([right, left, top, bottom, front, back]);
