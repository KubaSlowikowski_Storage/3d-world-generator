import {initializeRenderer} from "./helpers/renderer";
import {SceneContext} from "./helpers/sceneContext";
import {CameraContext} from "./helpers/camera";
import {TerrainGenerator} from "./terrain-generation/terrainGenerator";

export function initialize(animate) {
  // Renderer
  const renderer = initializeRenderer(animate);

  // Scene
  const sceneContext = new SceneContext();
  sceneContext.addLights();

  const {scene, stats} = sceneContext;

  // Stats
  document.body.appendChild(stats.dom);

  // Camera
  const {camera} = new CameraContext(renderer);

  const terrainGenerator = new TerrainGenerator(scene);

  return {renderer, scene, camera, stats, terrainGenerator};
}
