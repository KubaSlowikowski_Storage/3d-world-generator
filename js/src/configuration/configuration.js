export const CHUNK_SIZE = 125;
export const DENSITY_FACTOR = 1;
export const TRIANGLE_DENSITY = CHUNK_SIZE * DENSITY_FACTOR;
export const ADD_CHUNKS = true;
// export const ADD_CHUNKS = false;
export let ANTIALIAS_ENABLED = false;
export const SHADOWS_ENABLED = false;
export const GOOD_LOOKING_WORLD = false;
export const SKY_ENABLED = GOOD_LOOKING_WORLD;
export const FOG = {
  enabled: GOOD_LOOKING_WORLD,
  near: 90,
  far: 300,
};

export const WATER = {
  enabled: GOOD_LOOKING_WORLD,
  resolution: 100,
  height: 4.75
}
export const TERRAIN_WIREFRAME = false;

export const AXES_HELPER = false;

export const CAMERA_CONFIG = {
  fov: 45,
  near: 0.1,
  far: 500,
  position_x: 100,
  position_y: 35,
  position_z: 0
};

// export const SEED = "dsads#hs1"; // zawracająca rzeka
// export const SEED = "5ahf3#d1%ha"; // dobrze wygladajace systemy rzek
export const SEED = 132.623674371277487;
// export const SEED = Math.random();

export const RIVER_SOURCE_HEIGHT = {
  min: 8,
  max: 9
}
export const RIVER_MAX_STEPS = 250 * DENSITY_FACTOR;
export const RIVER_BASINS_ENABLED = false;
export const RIVER_BASINS_STEP = 75 * DENSITY_FACTOR;
export const RIVER_BASIN_MAX_STEPS = 100 * DENSITY_FACTOR;
export const RIVER_BASIN_SOURCE_DISTANCE = CHUNK_SIZE / 15;
