import {getYOnCurveByX} from "../utils/utils";
import * as THREE from "three";

export const EROSION_RATIO = {
  value: 5
};

const points = [
  new THREE.Vector3(-1, 1, 0),
  new THREE.Vector3(-0.66, 0.75, 0),
  new THREE.Vector3(-0.333, 0.58, 0),
  new THREE.Vector3(-0.28, 0.5, 0),
  new THREE.Vector3(0, 0.14, 0),
  new THREE.Vector3(0.33, 0.12, 0),
  new THREE.Vector3(0.77, 0.11, 0),
  new THREE.Vector3(0.8, 0.3, 0),
  new THREE.Vector3(0.83, 0.3, 0),
  new THREE.Vector3(0.88, 0.11, 0),
  new THREE.Vector3(0.95, 0.05, 0),
  new THREE.Vector3(1, 0.05, 0),
];

const curve = new THREE.CatmullRomCurve3(points);
const curvePoints = curve.getPoints(100);

export function getHeightByErosion(erosion, delta = 0.0025) {
  const height = getYOnCurveByX(erosion, curvePoints, delta);
  return height * EROSION_RATIO.value;
}
