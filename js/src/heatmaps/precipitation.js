import {getYOnCurveByX} from "../utils/utils";
import * as THREE from "three";

export const MIN_PRECIPITATION = 0;
export const MAX_PRECIPITATION = 400;

const points = [
  new THREE.Vector3(-1, 0, 0),
  new THREE.Vector3(-0.7, 40, 0),
  new THREE.Vector3(0, 100, 0),
  new THREE.Vector3(0.15, 110, 0),
  new THREE.Vector3(0.5, 200, 0),
  new THREE.Vector3(0.70, 350, 0),
  new THREE.Vector3(0.8, 380, 0),
  new THREE.Vector3(0.81, 390, 0),
  new THREE.Vector3(1, 400, 0),
];

const curve = new THREE.CatmullRomCurve3(points);
const curvePoints = curve.getPoints(100);

export function getPrecipitationByNoiseValue(noiseValue, delta = 0.0025) {
  return getYOnCurveByX(noiseValue, curvePoints, delta);
}
