import * as THREE from "three";
import {getYOnCurveByX} from "../utils/utils";

export const PV_RATIO = {
  value: 10
};

const points = [
  new THREE.Vector3(-1, 0, 0),
  new THREE.Vector3(0.2, 0, 0),
  new THREE.Vector3(0.35, 0.1, 0),
  new THREE.Vector3(0.36, 0.1, 0),
  new THREE.Vector3(0.6, 0.3, 0),
  new THREE.Vector3(0.7, 0.4, 0),
  new THREE.Vector3(0.9, 0.55, 0),
  new THREE.Vector3(1, 0.74, 0),
]

const curve = new THREE.CatmullRomCurve3(points);
const curvePoints = curve.getPoints(100);

export function getHeightByPV(pv, delta = 0.0025) {
  let height = getYOnCurveByX(pv, curvePoints, delta);
  return height * PV_RATIO.value;
}
