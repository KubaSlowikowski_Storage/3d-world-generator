import * as THREE from "three";
import {getYOnCurveByX} from "../utils/utils";

/**
 * CONTINENTALNESS DEFINES A WHOLE CONTINENT SHAPES.
 */

/**
 * Describes how many times we want to multiply height value.
 * @type {number}
 */
export const CONTINENTALNESS_RATIO = {
  value: 5
};

/**
 * Describes how fast terrain height should grow by continentalness factor.
 * @type {Vector3[]}
 */
const points = [
  new THREE.Vector3(-1, 1, 0),
  new THREE.Vector3(-0.36, 0.035, 0),
  new THREE.Vector3(-0.044, 0.88, 0),
  new THREE.Vector3(-0.035, 0.88, 0),
  new THREE.Vector3(0.001, 0.91, 0),
  new THREE.Vector3(0.52, 0.94, 0),
  new THREE.Vector3(1, 1, 0),
];


const curve = new THREE.CatmullRomCurve3(points);
const curvePoints = curve.getPoints(100);

export function getHeightByContinentalness(continentalness, delta = 0.0025) {
  const height = getYOnCurveByX(continentalness, curvePoints, delta);
  return height * CONTINENTALNESS_RATIO.value;
}
