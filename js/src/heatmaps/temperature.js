import {getYOnCurveByX} from "../utils/utils";
import * as THREE from "three";

export const MIN_TEMPERATURE = -10;
export const MAX_TEMPERATURE = 33;

const points = [
  new THREE.Vector3(-1, -10, 0),
  new THREE.Vector3(-0.5, 0, 0),
  new THREE.Vector3(-0.3, 10, 0),
  new THREE.Vector3(0.33, 23, 0),
  new THREE.Vector3(0.5, 30, 0),
  new THREE.Vector3(1, 33, 0),
];

const curve = new THREE.CatmullRomCurve3(points);
const curvePoints = curve.getPoints(100);

export function getTemperatureByNoiseValue(noiseValue, delta = 0.0025) {
  return getYOnCurveByX(noiseValue, curvePoints, delta);
}
