import * as THREE from "three";
import {
  CHUNK_SIZE,
  RIVER_BASIN_MAX_STEPS,
  RIVER_BASIN_SOURCE_DISTANCE,
  RIVER_BASINS_ENABLED,
  RIVER_BASINS_STEP,
  RIVER_MAX_STEPS,
  RIVER_SOURCE_HEIGHT,
  TRIANGLE_DENSITY,
  WATER,
} from "../configuration/configuration";

export class RiverGenerator {
  constructor() {
    this.edgePoints = [];
    this.delta = CHUNK_SIZE / TRIANGLE_DENSITY;
    // this.chunksToRebuild = [];
    this.globalRiverPoints = [];
  }

  createRiverTexture(width, height, continentHeights, erosionHeights, pvHeights, startFromEdgePointOnly = false) {
    this.textureSize = width * height;
    this.continentHeights = continentHeights;
    this.erosionHeights = erosionHeights;
    this.pvHeights = pvHeights;
    const riverPath = [];
    if (!startFromEdgePointOnly) {
      riverPath.push(...this.createRiverPath());
    }
    this.edgePoints.forEach(_ => riverPath.push(...this.createRiverPath(false, true)));

    // Draw river on data texture
    const riverData = new Uint8Array(4 * this.textureSize);
    for (let i = 0; i < this.textureSize; i++) {
      let river;
      const stride = i * 4;
      const x = continentHeights[i].global_x;
      const y = continentHeights[i].global_y;
      if (riverPath.find(point => point.x === x && point.y === y)) {
        river = true;
      }

      riverData[stride] = river ? 1 : 0;
      riverData[stride + 1] = 0;
      riverData[stride + 2] = 0;
      riverData[stride + 3] = 255; // alpha
    }

    const texture = new THREE.DataTexture(riverData, width, height);
    texture.needsUpdate = true;

    return texture;
  }

  createRiverPath(basin = false, startFromEdgePointOnly = false, initialPoint) {
    const startPoint = initialPoint ? initialPoint : this.findRiverStartPoint(startFromEdgePointOnly);
    if (!startPoint) {
      return [];
    }

    const riverPath = [startPoint];
    const basinsPath = [];

    let currentPoint = startPoint;
    let step = 0;
    if (basin) {
      step = RIVER_MAX_STEPS - RIVER_BASIN_MAX_STEPS;
    }

    while (!riverShouldStop()) {
      step++;

      if (shouldCreateRiverBasin()) {
        basinsPath.push(...this.createRiverPath(true));
      }

      if (RIVER_BASINS_ENABLED && step === 15) {
        basinsPath.push(...this.createRiverPath(true, false, this.findPointByDistanceFrom(RIVER_BASIN_SOURCE_DISTANCE, currentPoint)));
      }

      const neighbours = this.findNeighbours(currentPoint, 5);

      let nextPoint = this.findNextPoint(currentPoint, neighbours, riverPath, basin);
      if (!nextPoint) {
        nextPoint = riverPath[step - 1];
      }

      riverPath.push(nextPoint);
      this.globalRiverPoints.push(nextPoint);
      currentPoint = nextPoint;

      if (currentPoint && this.isPointOnChunkEdge(currentPoint)) {
        if (!edgePointExists(this.edgePoints, currentPoint)) {
          this.edgePoints.push(currentPoint);
        }
        break;
      }
    }

    riverPath.push(...basinsPath);
    return riverPath;

    function riverShouldStop() {
      return step >= RIVER_MAX_STEPS || currentPoint.z < WATER.height;
    }

    function edgePointExists(edgePoints, point) {
      return edgePoints.find(p => p.x === point.x && p.y === point.y);
    }

    function shouldCreateRiverBasin() {
      return RIVER_BASINS_ENABLED && !basin && (step === RIVER_BASINS_STEP || step === 2 * RIVER_BASINS_STEP);
    }
  }

  findPointByDistanceFrom(distance, from) {
    console.log('started')
    for (let i = 0; i < this.textureSize; i++) {
      const x = this.continentHeights[i].global_x;
      const y = this.continentHeights[i].global_y;
      const terrainHeight = this.continentHeights[i].value + this.erosionHeights[i].value + this.pvHeights[i].value;

      const point = new THREE.Vector2(x, y);
      let dst = point.distanceTo(new THREE.Vector2(from.x, from.y));
      if (terrainHeight > WATER.height && dst >= distance && dst <= (distance * 1.5)) {
        console.log('basin start point: ', new THREE.Vector3(x, y, terrainHeight));
        return new THREE.Vector3(x, y, terrainHeight);
      }
    }
  }

  findRiverStartPoint(startFromEdgePointOnly) {
    // Start from edge point
    for (let i = 0; i < this.textureSize; i++) {
      const x = this.continentHeights[i].global_x;
      const y = this.continentHeights[i].global_y;
      const terrainHeight = this.continentHeights[i].value + this.erosionHeights[i].value + this.pvHeights[i].value;

      for (let edgePoint of this.edgePoints) {
        const point = new THREE.Vector2(x, y);
        let distance = point.distanceTo(new THREE.Vector2(edgePoint.x, edgePoint.y));
        if (terrainHeight > WATER.height && distance <= 0.9 * (this.delta)) {
          if (Math.random() > 0.5) { // FIXME
            this.edgePoints = this.edgePoints.filter(ep => ep.x !== edgePoint.x && ep.y !== edgePoint.y); // remove edge point
          }
          return new THREE.Vector3(x, y, terrainHeight);
        }
      }
    }

    if (startFromEdgePointOnly) {
      return;
    }

    // Otherwise, start from random point
    for (let i = 0; i < this.textureSize; i++) {
      const x = this.continentHeights[i].global_x;
      const y = this.continentHeights[i].global_y;
      const terrainHeight = this.continentHeights[i].value + this.erosionHeights[i].value + this.pvHeights[i].value;

      if (terrainHeight > RIVER_SOURCE_HEIGHT.min && terrainHeight < RIVER_SOURCE_HEIGHT.max && !this.isPointOnChunkEdge(new THREE.Vector2(x, y))) {
        return new THREE.Vector3(x, y, terrainHeight);
      }
    }
  }

  findNeighbours(pt, levels = 3) {
    const reference = new THREE.Vector2(pt.x, pt.y);
    const neighbours = [];

    for (let i = 0; i < this.textureSize; i++) {
      const x = this.continentHeights[i].global_x;
      const y = this.continentHeights[i].global_y;
      const z = this.continentHeights[i].value + this.erosionHeights[i].value + this.pvHeights[i].value;
      const point = new THREE.Vector2(x, y);

      for (let parentLevel = 1; parentLevel <= levels; parentLevel++) {
        if (point.distanceTo(reference) <= parentLevel * 1.4 * this.delta) {
          neighbours.push({
            point: new THREE.Vector3(x, y, z),
            weight: 1 - (0.1 * (parentLevel - 1))
          });
        }
      }
    }
    return neighbours;
  }

  findNextPoint(referencePoint, neighbours, riverPoints, basin = false) {
    alreadyInPath = alreadyInPath.bind(this);

    let nextPoint;

    if (basin) {
      findHigherPoint();
      if (!pointFound()) {
        findLowestPoint();
      }
    } else {
      findLowestPoint();
      if (!pointFound()) {
        findHigherPoint();
      }
    }

    if (nextPoint && nextPoint.distanceTo(riverPoints[riverPoints.length - 1]) >= 1.4 * this.delta) {
      fillGap(riverPoints[riverPoints.length - 1], nextPoint);
    }

    return nextPoint;

    function alreadyInPath(neighbour) {
      const existsInCurrentRiver = riverPoints.findIndex(p => p.x === neighbour.x && p.y === neighbour.y) !== -1;
      const existsGlobally = this.globalRiverPoints.findIndex(p => p.x === neighbour.x && p.y === neighbour.y) !== -1;

      return existsInCurrentRiver || existsGlobally;
    }

    function findLowestPoint() {
      const ranking = [];
      for (let i = 0; i < neighbours.length; i++) {
        const neighbourPoint = neighbours[i].point;
        const {weight} = neighbours[i];

        if (alreadyInPath(neighbourPoint)) {
          continue;
        }

        const descent = referencePoint.z - neighbourPoint.z;
        ranking.push({
          point: neighbourPoint,
          score: descent * weight
        });
      }

      const sortedRanking = ranking.sort((a, b) => {
        if (a.score < b.score) return 1;
        if (a.score > b.score) return -1;
        return 0;
      });

      nextPoint = sortedRanking[0] ? sortedRanking[0].point : undefined;
    }

    function findHigherPoint() {
      const ranking = [];
      for (let i = 0; i < neighbours.length; i++) {
        const neighbourPoint = neighbours[i].point;
        const {weight} = neighbours[i];

        if (alreadyInPath(neighbourPoint)) {
          continue;
        }
        if (weight !== 1 || weight !== 0.9) {
          continue;
        }

        const climb = referencePoint.z - neighbourPoint.z;
        ranking.push({
          point: neighbourPoint,
          score: weight / climb
        });
      }

      const sortedRanking = ranking.sort((a, b) => {
        if (a.score < b.score) return 1;
        if (a.score > b.score) return -1;
        return 0;
      });

      nextPoint = sortedRanking[0] ? sortedRanking[0].point : undefined;
    }

    function pointFound() {
      return nextPoint !== undefined;
    }

    function fillGap(from, to) {
      let fromVec2 = new THREE.Vector2(from.x, from.y);
      const pointSize = CHUNK_SIZE / TRIANGLE_DENSITY;

      const direction = new THREE.Vector3().subVectors(from, to);
      const points_x = direction.x / pointSize;
      const points_y = direction.y / pointSize;

      const gapPoints = [];
      for (let i = 1; i <= Math.abs(points_x); i++) {
        let point;
        if (points_x > 0) {
          point = fromVec2.add(new THREE.Vector2(-pointSize, 0));
        } else {
          point = fromVec2.add(new THREE.Vector2(pointSize, 0));
        }
        if (point.x !== to.x || point.y !== to.y) {
          gapPoints.push(point.clone());
        }
      }

      for (let i = 1; i <= Math.abs(points_y); i++) {
        let point;
        if (points_y > 0) {
          point = fromVec2.add(new THREE.Vector2(0, -pointSize));
        } else {
          point = fromVec2.add(new THREE.Vector2(0, pointSize));
        }
        if (point.x !== to.x || point.y !== to.y) {
          gapPoints.push(point.clone());
        }
      }

      riverPoints.push(...gapPoints);
    }
  }

  isPointOnChunkEdge(point) {
    const {x, y} = point;
    return Math.abs(x) === CHUNK_SIZE / 2 || (Math.abs(x) + CHUNK_SIZE / 2) % (CHUNK_SIZE) === 0 ||
      Math.abs(y) === CHUNK_SIZE / 2 || (Math.abs(y) + CHUNK_SIZE / 2) % (CHUNK_SIZE) === 0;
  }
}
