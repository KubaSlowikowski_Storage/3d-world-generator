import * as THREE from "three";
import {
  continentalnessFbmParams,
  erosionFbmParams,
  getPerlinNoise,
  peaks_and_valleys_FbmParams,
  precipitationFbmParams,
  temperatureFbmParams
} from "../noise/noise";
import {
  CHUNK_SIZE,
  SEED,
  SHADOWS_ENABLED,
  TERRAIN_WIREFRAME,
  TRIANGLE_DENSITY, WATER,
} from "../configuration/configuration";
import {continentalnessFragmentShader} from '../shaders/terrain/continentalnessShaders';
import {getHeightByContinentalness} from "../heatmaps/continentalness";
import {erosionFragmentShader} from "../shaders/terrain/erosionShaders";
import {getHeightByErosion} from "../heatmaps/erosion";
import {terrainFragmentShader, terrainVertexShader} from "../shaders/terrain/terrainShaders";
import {getHeightByPV} from "../heatmaps/peaksAndValleys";
import {peakAndValleysFragmentShader} from "../shaders/terrain/peaksAndVallesShaders";
import {getTemperatureByNoiseValue, MAX_TEMPERATURE, MIN_TEMPERATURE} from "../heatmaps/temperature";
import {getPrecipitationByNoiseValue, MAX_PRECIPITATION, MIN_PRECIPITATION} from "../heatmaps/precipitation";
import {temperatureFragmentShader} from "../shaders/climate/temperatureShaders";
import {precipitationFragmentShader} from "../shaders/climate/precipitationShaders";
import {heatmapVertexShader} from "../shaders/heatmapVertexShader";
import {texturesUniformData} from "./biomes/textures";
import {verticalMirrorArray} from "../utils/utils";
import {RiverGenerator} from "./riverGenerator";
import {fogUniformData} from "./biomes/fog";
import {BiomeGenerator} from "./biomeGenerator";
import {Water} from "../utils/water/water";
import flowMapImage from '../../../img/Water_2_M_Normal.jpg';

export class TerrainGenerator {

  constructor(scene) {
    this.scene = scene;
    this.chunks = [];

    this.continentalnessNoise = getPerlinNoise(SEED);
    this.erosionNoise = getPerlinNoise(SEED + SEED);
    this.pvNoise = getPerlinNoise(SEED + SEED + SEED);
    this.temperatureNoise = getPerlinNoise(SEED + SEED + SEED + SEED);
    this.precipitationNoise = getPerlinNoise(SEED + SEED + SEED + SEED + SEED);

    this.temperatures = [];
    this.precipitations = [];
    this.continentalnessHeights = [];
    this.erosionHeights = [];
    this.pvHeights = [];

    this.globalContinentalnessHeights = [];
    this.globalErosionHeights = [];
    this.globalPvHeights = [];

    this.riverGenerator = new RiverGenerator();
    this.biomeGenerator = new BiomeGenerator();

    this.formTerrain = this.formTerrain.bind(this);
    this.createContinentalnessHeatMap = this.createContinentalnessHeatMap.bind(this);
    this.createErosionHeatMap = this.createErosionHeatMap.bind(this);
    this.createPV_Heatmap = this.createPV_Heatmap.bind(this);
    this.createTemperatureHeatmap = this.createTemperatureHeatmap.bind(this);
    this.createPrecipitationHeatmap = this.createPrecipitationHeatmap.bind(this);

    this.computeContinentalnessFBM = this.computeContinentalnessFBM.bind(this);
    this.computeErosionFBM = this.computeErosionFBM.bind(this);
    this.computePV_FBM = this.computePV_FBM.bind(this);
    this.computeTemperatureFBM = this.computeTemperatureFBM.bind(this);
    this.computePrecipitationFBM = this.computePrecipitationFBM.bind(this);

    const textureLoader = new THREE.TextureLoader();
    this.flowMap = textureLoader.load(flowMapImage);
  }

  createChunk(x, y) {
    const continentalnessChunk = this.createContinentalnessChunk(x, y);
    const erosionChunk = this.createErosionChunk(x, y);
    const pvChunk = this.createPV_Chunk(x, y);
    const temperatureChunk = this.createTemperatureChunk(x, y);
    const precipitationChunk = this.createPrecipitationChunk(x, y);

    const terrainChunk = this.createTerrainChunk(x, y);

    this.chunks.push({
      chunk: {
        terrainChunk: terrainChunk,
        continentalnessChunk: continentalnessChunk,
        erosionChunk: erosionChunk,
        pvChunk: pvChunk,
        temperatureChunk: temperatureChunk,
        precipitationChunk: precipitationChunk
      },
      x: x,
      y: y
    });

    return {
      terrainChunk: terrainChunk,
      continentalnessChunk: continentalnessChunk,
      erosionChunk: erosionChunk,
      pvChunk: pvChunk,
      temperatureChunk: temperatureChunk,
      precipitationChunk: precipitationChunk
    };
  }

  createTerrainChunk(x, y) {
    const planeGeometry = new THREE.PlaneGeometry(CHUNK_SIZE, CHUNK_SIZE, TRIANGLE_DENSITY, TRIANGLE_DENSITY);

    this.mirrorHeights();

    const planeMaterial = this.createTerrainMaterial();
    const plane = new THREE.Mesh(planeGeometry, planeMaterial);
    plane.rotation.x = -Math.PI * 0.5;
    plane.position.set(x * CHUNK_SIZE, 0, y * CHUNK_SIZE); // assigning a 'z' value, because plane is rotated by -90 degrees

    if (SHADOWS_ENABLED) {
      plane.receiveShadow = true;
      plane.castShadow = true;
    }

    this.formTerrain(plane);
    this.scene.add(plane);

    if (WATER.enabled) {
      const waterGeometry = new THREE.PlaneGeometry(CHUNK_SIZE, CHUNK_SIZE, 1, 1);
      const water = new Water(waterGeometry, {
        scale: 10,
        flowSpeed: 0.005,
        textureWidth: WATER.resolution,
        textureHeight: WATER.resolution,
        reflectivity: 0.05
      });
      water.rotation.x = -Math.PI * 0.5;
      water.position.set(x * CHUNK_SIZE, WATER.height, y * CHUNK_SIZE);
      this.scene.add(water);
    }

    return plane;
  }

  createContinentalnessChunk(x, y) {
    return this.buildChunk(x, y, this.createContinentalnessHeatMap, false, heatmapVertexShader, continentalnessFragmentShader);
  }

  createErosionChunk(x, y) {
    return this.buildChunk(x, y, this.createErosionHeatMap, false, heatmapVertexShader, erosionFragmentShader);
  }

  createPV_Chunk(x, y) {
    return this.buildChunk(x, y, this.createPV_Heatmap, false, heatmapVertexShader, peakAndValleysFragmentShader);
  }

  createTemperatureChunk(x, y) {
    return this.buildChunk(x, y, this.createTemperatureHeatmap, false, heatmapVertexShader, temperatureFragmentShader);
  }

  createPrecipitationChunk(x, y) {
    return this.buildChunk(x, y, this.createPrecipitationHeatmap, false, heatmapVertexShader, precipitationFragmentShader);
  }

  buildChunk(x, y, createHeatmap, visible = true, vertexShader, fragmentShader) {
    const planeGeometry = new THREE.PlaneGeometry(CHUNK_SIZE, CHUNK_SIZE, TRIANGLE_DENSITY, TRIANGLE_DENSITY);
    const planeMaterial = new THREE.ShaderMaterial({
      visible: visible,
      wireframe: true,
      vertexShader: vertexShader,
      fragmentShader: fragmentShader
    });
    return this.createPlane(x, y, planeGeometry, planeMaterial, createHeatmap);
  }

  createPlane(x, y, planeGeometry, planeMaterial, createHeatmap) {
    const plane = new THREE.Mesh(planeGeometry, planeMaterial);
    plane.rotation.x = -Math.PI * 0.5;
    plane.position.set(x * CHUNK_SIZE, 0, y * CHUNK_SIZE); // assigning a 'z' value, because plane is rotated by -90 degrees

    createHeatmap(plane);
    this.scene.add(plane);
    return plane;
  }

  rebuildChunks() {
    this.rebuildContinentalnessChunks();
    this.rebuildErosionChunks();
    this.rebuildPV_Chunks();
    this.rebuildTemperatureChunks();
    this.rebuildPrecipitationChunks();

    this.rebuildTerrainChunks();
  }

  rebuildContinentalnessChunks() {
    this.chunks.forEach(c => {
      this.createContinentalnessHeatMap(c.chunk.continentalnessChunk);
    });
  }

  rebuildErosionChunks() {
    this.chunks.forEach(c => {
      this.createErosionHeatMap(c.chunk.erosionChunk);
    });
  }

  rebuildTerrainChunks() {
    this.chunks.forEach(c => {
      this.formTerrain(c.chunk.terrainChunk);
    });
  }

  rebuildPV_Chunks() {
    this.chunks.forEach(c => {
      this.createPV_Heatmap(c.chunk.pvChunk);
    })
  }

  rebuildTemperatureChunks() {
    this.chunks.forEach(c => {
      this.createTemperatureHeatmap(c.chunk.temperatureChunk);
    })
  }

  rebuildPrecipitationChunks() {
    this.chunks.forEach(c => {
      this.createPrecipitationHeatmap(c.chunk.precipitationChunk);
    })
  }

  formTerrain(plane) {
    const positionAttribute = plane.geometry.attributes.position;
    const meshPosition = plane.position;

    for (let i = 0; i < positionAttribute.count; i++) {
      const vertex = new THREE.Vector3();
      vertex.fromBufferAttribute(positionAttribute, i);

      const global_x = vertex.x + meshPosition.x;
      const global_y = vertex.y - meshPosition.z;

      const continentalness = this.computeContinentalnessFBM(global_x, global_y);
      const continentalnessHeight = getHeightByContinentalness(continentalness);

      const erosion = this.computeErosionFBM(global_x, global_y);
      const erosionHeight = getHeightByErosion(erosion);

      const peaks_and_valleys = this.computePV_FBM(global_x, global_y);
      const peaks_and_valleys_height = getHeightByPV(peaks_and_valleys);

      let totalHeight = continentalnessHeight + erosionHeight + peaks_and_valleys_height;
      vertex.setZ(totalHeight);

      positionAttribute.setXYZ(i, vertex.x, vertex.y, vertex.z);
    }

    positionAttribute.needsUpdate = true;
  }

  createTerrainMaterial(reCreate = false) {
    const width = TRIANGLE_DENSITY + 1;
    const height = TRIANGLE_DENSITY + 1;
    return new THREE.ShaderMaterial({
      wireframe: TERRAIN_WIREFRAME,
      uniforms: {
        climateTexture: {value: this.biomeGenerator.createClimateTexture(this.temperatures, this.precipitations)},
        riversMap: {value: this.riverGenerator.createRiverTexture(width, height, this.continentalnessHeights, this.erosionHeights, this.pvHeights, reCreate)},
        waterLevel: {type: 'float', value: WATER.height},
        chunk_size: {value: CHUNK_SIZE},
        ...texturesUniformData,
        ...fogUniformData
      },
      vertexShader: terrainVertexShader,
      fragmentShader: terrainFragmentShader,
      visible: true
    });
  }

  formRivers(plane) {

  }

  createContinentalnessHeatMap(plane) {
    this.createHeatmap(plane, this.computeContinentalnessFBM, getHeightByContinentalness, this.continentalnessHeights, this.globalContinentalnessHeights)
  }

  createErosionHeatMap(plane) {
    this.createHeatmap(plane, this.computeErosionFBM, getHeightByErosion, this.erosionHeights, this.globalErosionHeights);
  }

  createPV_Heatmap(plane) {
    this.createHeatmap(plane, this.computePV_FBM, getHeightByPV, this.pvHeights, this.globalPvHeights);
  }

  createTemperatureHeatmap(plane) {
    this.temperatures = [];
    const positionAttribute = plane.geometry.attributes.position;
    const meshPosition = plane.position;

    for (let i = 0; i < positionAttribute.count; i++) {
      const vertex = new THREE.Vector3();
      vertex.fromBufferAttribute(positionAttribute, i);

      const global_x = vertex.x + meshPosition.x;
      const global_y = vertex.y - meshPosition.z;
      const factor = this.computeTemperatureFBM(global_x, global_y);
      let temperature = getTemperatureByNoiseValue(factor);

      this.temperatures.push(temperature);
      vertex.setZ((temperature + Math.abs(MIN_TEMPERATURE)) / MAX_TEMPERATURE);

      positionAttribute.setXYZ(i, vertex.x, vertex.y, vertex.z);
    }

    positionAttribute.needsUpdate = true;
  }

  createPrecipitationHeatmap(plane) {
    this.precipitations = [];
    const positionAttribute = plane.geometry.attributes.position;
    const meshPosition = plane.position;

    for (let i = 0; i < positionAttribute.count; i++) {
      const vertex = new THREE.Vector3();
      vertex.fromBufferAttribute(positionAttribute, i);

      const global_x = vertex.x + meshPosition.x;
      const global_y = vertex.y - meshPosition.z;
      const factor = this.computePrecipitationFBM(global_x, global_y);
      let precipitation = getPrecipitationByNoiseValue(factor);

      this.precipitations.push(precipitation);
      vertex.setZ((precipitation + MIN_PRECIPITATION) / MAX_PRECIPITATION);

      positionAttribute.setXYZ(i, vertex.x, vertex.y, vertex.z);
    }

    positionAttribute.needsUpdate = true;
  }

  createHeatmap(plane, fbmFunction, getHeightFunction, array = [], globalArray = []) {
    array.length = 0;
    const positionAttribute = plane.geometry.attributes.position;
    const meshPosition = plane.position;
    for (let i = 0; i < positionAttribute.count; i++) {
      const vertex = new THREE.Vector3();
      vertex.fromBufferAttribute(positionAttribute, i);

      const global_x = vertex.x + meshPosition.x;
      const global_y = vertex.y - meshPosition.z;
      const factor = fbmFunction(global_x, global_y);
      let z = getHeightFunction(factor);

      array.push({global_x: global_x, global_y: global_y, value: z});
      globalArray.push({global_x: global_x, global_y: global_y, value: z});
      vertex.setZ(z);

      positionAttribute.setXYZ(i, vertex.x, vertex.y, vertex.z);
    }
    positionAttribute.needsUpdate = true;
  }

  computeContinentalnessFBM(x, y) {
    return this.computeFBM(x, y, continentalnessFbmParams, this.continentalnessNoise);
  }

  computeErosionFBM(x, y) {
    return this.computeFBM(x, y, erosionFbmParams, this.erosionNoise);
  }

  computePV_FBM(x, y) {
    return this.computeFBM(x, y, peaks_and_valleys_FbmParams, this.pvNoise);
  }

  computeTemperatureFBM(x, y) {
    return this.computeFBM(x, y, temperatureFbmParams, this.temperatureNoise);
  }

  computePrecipitationFBM(x, y) {
    return this.computeFBM(x, y, precipitationFbmParams, this.precipitationNoise);
  }

  /**
   * Computes Fractional Brownian Motion of the given noise.
   * @param x X coordinate of the point
   * @param y Y coordinate of the point
   * @param params FBM params
   * @param noiseFunction noise function
   * @returns {number} Z coordinate of the given point
   */
  computeFBM(x, y, params, noiseFunction) {
    let {octaves, frequency, lacunarity, persistence} = params;
    let normalization = 0;
    let amplitude = 1;
    let total = 0;
    for (let octave = 0; octave < octaves; octave++) {
      const noise = noiseFunction(x * frequency, y * frequency);
      total += noise * amplitude;
      normalization += amplitude
      frequency *= lacunarity;
      amplitude *= persistence;
    }

    total /= normalization;

    return total;
  }

  mirrorHeights() {
    const width = TRIANGLE_DENSITY + 1;
    const height = TRIANGLE_DENSITY + 1;

    this.temperatures = verticalMirrorArray(this.temperatures, width, height);
    this.precipitations = verticalMirrorArray(this.precipitations, width, height);
    this.continentalnessHeights = verticalMirrorArray(this.continentalnessHeights, width, height);
    this.erosionHeights = verticalMirrorArray(this.erosionHeights, width, height);
    this.pvHeights = verticalMirrorArray(this.pvHeights, width, height);
  }
}
