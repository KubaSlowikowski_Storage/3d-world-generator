import * as THREE from "three";
import water_image from '/img/biomes/water.jpg';
import boreal_forest_image from '/img/biomes/boreal_forest.jpg';
import savanna_image from '/img/biomes/savanna.jpg';
import subtropical_dessert_image from '/img/biomes/subtropical_desert.jpg';
import temperate_grassland_cold_dessert_image from '/img/biomes/temperate_grassland_cold_dessert.jpg';
import temperate_rainforest_image from '/img/biomes/temperate_rainforest.jpg';
import temperate_seasonal_forest_image from '/img/biomes/temperate_seasonal_forest.jpg';
import tropical_rainforest_image from '/img/biomes/tropical_rainforest.jpg';
import tundra_image from '/img/biomes/tundra.jpg';
import woodland_shrubland_image from '/img/biomes/woodland_shrubland.jpg';
import river_image from '/img/biomes/river.jpg';
import snow_image from '/img/snow.png';
import rocks_image from '/img/rocks.png';

const textureLoader = new THREE.TextureLoader();

const water_texture = textureLoader.load(water_image);

const boreal_forest_texture = textureLoader.load(boreal_forest_image);
const savanna_texture = textureLoader.load(savanna_image);
const subtropical_dessert_texture = textureLoader.load(subtropical_dessert_image);
const temperate_grassland_cold_dessert_texture = textureLoader.load(temperate_grassland_cold_dessert_image);
const temperate_seasonal_forest_texture = textureLoader.load(temperate_seasonal_forest_image);
const temperate_rainforest_texture = textureLoader.load(temperate_rainforest_image);
const tropical_rainforest_texture = textureLoader.load(tropical_rainforest_image);
const tundra_rainforest_texture = textureLoader.load(tundra_image);
const woodland_shrubland_texture = textureLoader.load(woodland_shrubland_image);
const river_texture = textureLoader.load(river_image);
const snow_texture = textureLoader.load(snow_image);
const rocks_texture = textureLoader.load(rocks_image);

export const texturesUniformData = {
  water_texture: {
    value: water_texture
  },
  boreal_forest_texture: {
    value: boreal_forest_texture
  },
  savanna_texture: {
    value: savanna_texture
  },
  subtropical_dessert_texture: {
    value: subtropical_dessert_texture
  },
  temperate_grassland_cold_dessert_texture: {
    value: temperate_grassland_cold_dessert_texture
  },
  temperate_seasonal_forest_texture: {
    value: temperate_seasonal_forest_texture
  },
  temperate_rainforest_texture: {
    value: temperate_rainforest_texture
  },
  tropical_rainforest_texture: {
    value: tropical_rainforest_texture
  },
  tundra_rainforest_texture: {
    value: tundra_rainforest_texture
  },
  woodland_shrubland_texture: {
    value: woodland_shrubland_texture
  },
  river_texture: {
    value: river_texture
  },
  snow_texture: {
    value: snow_texture
  },
  rocks_texture: {
    value: rocks_texture
  }
}
