import {isInRange} from "../../utils/utils";

export const BIOMES = {
  TUNDRA: 1,
  BOREAL_FOREST: 2,
  TEMPERATE_GRASSLAND_COLD_DESERT: 3,
  WOODLAND_SHRUBLAND: 4,
  SUBTROPICAL_DESERT: 5,
  TEMPERATE_SEASONAL_FOREST: 6,
  TROPICAL_SEASONAL_FOREST_SAVANNA: 7,
  TEMPERATE_RAINFOREST: 8,
  TROPICAL_RAINFOREST: 9,

  RIVER: 10
}

const biomes = {
  "Tundra": {"temperature_range": [-10, 0], "precipitation_range": [0, 100]},                             // 1
  "Boreal forest": {"temperature_range": [0, 8], "precipitation_range": [40, 200]},                       // 2
  "Temperate grassland / cold dessert": {"temperature_range": [0, 23], "precipitation_range": [0, 40]},   // 3
  "Woodland / shrubland": {"temperature_range": [8, 23], "precipitation_range": [40, 100]},               // 4
  "Subtropical dessert": {"temperature_range": [23, 33], "precipitation_range": [0, 100]},                // 5
  "Temperate seasonal forest": {"temperature_range": [8, 23], "precipitation_range": [100, 200]},         // 6
  "Tropical seasonal forest/savanna": {"temperature_range": [23, 33], "precipitation_range": [100, 240]}, // 7
  "Temperate rainforest": {"temperature_range": [8, 23], "precipitation_range": [200, 300]},              // 8
  "Tropical rainforest": {"temperature_range": [23, 33], "precipitation_range": [240, 400]},              // 9
};

export function getBiomeTypeByTemperature(temperature) {

  if (isBiomeByTemperature("Tundra")) return "Tundra";
  if (isBiomeByTemperature("Boreal forest")) return "Boreal forest";
  // if (isBiome("Temperate grassland / cold dessert")) return "Temperate grassland / cold dessert";
  // if (isBiome("Woodland / shrubland")) return "Woodland / shrubland";
  // if (isBiome("Subtropical dessert")) return "Subtropical dessert";
  // if (isBiome("Temperate seasonal forest")) return "Temperate seasonal forest";
  // if (isBiome("Tropical seasonal forest/savanna")) return "Tropical seasonal forest/savanna";
  if (isBiomeByTemperature("Temperate rainforest")) return "Temperate rainforest";
  if (isBiomeByTemperature("Tropical rainforest")) return "Tropical rainforest";

  function isBiomeByTemperature(biomeType) {
    const temperatureRange = getTemperatureRange(biomeType);
    return isInRange(temperature, temperatureRange.min, temperatureRange.max);
  }
}

export function limitTooHighPrecipitation(biomeType, precipitation) {
  const maxPrecipitation = getPrecipitationRange(biomeType).max;
  if (precipitation > maxPrecipitation) {
    precipitation = maxPrecipitation;
  }
  return precipitation;
}

function getPrecipitationRange(biomeType) {
  const range = biomes[biomeType].precipitation_range;
  return {
    min: range[0],
    max: range[1]
  };
}

function getTemperatureRange(biomeType) {
  const range = biomes[biomeType].temperature_range;
  return {
    min: range[0],
    max: range[1]
  };
}

function getRangesForBiome(biomeType) {
  return {
    temperature: getTemperatureRange(biomeType),
    precipitation: getPrecipitationRange(biomeType)
  }
}

export function getBiome(temperature, precipitation) {
  if (isBiome("Tundra", temperature, precipitation)) return BIOMES.TUNDRA;
  if (isBiome("Boreal forest", temperature, precipitation)) return BIOMES.BOREAL_FOREST;
  if (isBiome("Temperate grassland / cold dessert", temperature, precipitation)) return BIOMES.TEMPERATE_GRASSLAND_COLD_DESERT;
  if (isBiome("Woodland / shrubland", temperature, precipitation)) return BIOMES.WOODLAND_SHRUBLAND;
  if (isBiome("Subtropical dessert", temperature, precipitation)) return BIOMES.SUBTROPICAL_DESERT;
  if (isBiome("Temperate seasonal forest", temperature, precipitation)) return BIOMES.TEMPERATE_SEASONAL_FOREST;
  if (isBiome("Tropical seasonal forest/savanna", temperature, precipitation)) return BIOMES.TROPICAL_SEASONAL_FOREST_SAVANNA;
  if (isBiome("Temperate rainforest", temperature, precipitation)) return BIOMES.TEMPERATE_RAINFOREST;
  if (isBiome("Tropical rainforest", temperature, precipitation)) return BIOMES.TROPICAL_RAINFOREST;
}

function isBiome(biomeType, temperature, precipitation) {
  const range = getRangesForBiome(biomeType);
  const isTemperatureInRange = isInRange(temperature, range.temperature.min, range.temperature.max);
  const isPrecipitationInRange = isInRange(precipitation, range.precipitation.min, range.precipitation.max);

  return isTemperatureInRange && isPrecipitationInRange;
}
