import {FOG} from "../../configuration/configuration";

export const fogUniformData = {
  fogEnabled: {value: FOG.enabled},
  fogNear: {value: FOG.near},
  fogFar: {value: FOG.far},
}
