import {getBiome, getBiomeTypeByTemperature, limitTooHighPrecipitation} from "./biomes/biomes";
import * as THREE from "three";
import {TRIANGLE_DENSITY} from "../configuration/configuration";

export class BiomeGenerator {
  constructor() {
    this.textureWidth = TRIANGLE_DENSITY + 1;
    this.textureHeight = TRIANGLE_DENSITY + 1;
  }

  createClimateTexture(temperatures, precipitations) {
    const temperatureData = new Uint8Array(4 * this.textureWidth * this.textureHeight);
    for (let i = 0; i < this.textureWidth * this.textureHeight; i++) {
      const stride = i * 4;

      const temperature = temperatures[i];
      let precipitation = precipitations[i];
      precipitation = limitPrecipitation(temperature, precipitation);

      const biome = getBiome(temperature, precipitation);

      temperatureData[stride] = biome;
      temperatureData[stride + 1] = 0;
      temperatureData[stride + 2] = 0;
      temperatureData[stride + 3] = 255; // alpha
    }

    const texture = new THREE.DataTexture(temperatureData, this.textureWidth, this.textureHeight);
    texture.needsUpdate = true;

    return texture;

    // If annual precipitation is too high for a given temperature zone, it will be cut to the highest level allowed.
    function limitPrecipitation(temperature, precipitation) {
      const biomeType = getBiomeTypeByTemperature(temperature);
      return limitTooHighPrecipitation(biomeType, precipitation);
    }
  }
}
