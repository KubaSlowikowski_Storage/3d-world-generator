import * as THREE from "three";

const DEBUG = false;

export function limit(value, max, min) {
  if (value > max) value = max;
  if (value < min) value = min;

  return value;
}

export function getYOnCurveByX(x, curvePoints, delta = 0.005) {

  if (DEBUG) {
    console.log('X: ', x);
  }
  if (delta > 0.1) {
    return;
  }

  if (DEBUG) {
    console.log('Delta: ', delta);
  }

  let found = false;
  let result;
  for (let point of curvePoints) {
    if (Math.abs(point.x - x) < delta) {
      if (DEBUG) {
        console.log(point);
      }
      found = true;
      result = point;
      break;
    }
  }

  if (!found) {
    if (DEBUG) {
      console.log('Not found, increasing delta');
    }
    return getYOnCurveByX(x, curvePoints, delta * 2);
  }

  return result.y;
}

function drawCurve(scene) {
  const POINT_SIZE = 1;
  const points = [
    // new THREE.Vector3(-1, 2, 0),
    new THREE.Vector3(-1, 0.3, 0),
    new THREE.Vector3(0.2, 0.4, 0),
    new THREE.Vector3(0.25, 0.9, 0),
    new THREE.Vector3(1, 1.0, 0),
  ];

// create a Catmull-Rom spline curve
  const curve = new THREE.CatmullRomCurve3(points);

  const curvePoints = curve.getPoints(curve.arcLengthDivisions);
  const geometry = new THREE.BufferGeometry().setFromPoints(curvePoints);
  const material = new THREE.LineBasicMaterial({color: 'red'});
  const curveObject = new THREE.Line(geometry, material);
  scene.add(curveObject);

  points.forEach(point => {
    const dotGeometry = new THREE.BufferGeometry();
    dotGeometry.setAttribute('position', new THREE.BufferAttribute(new Float32Array([point.x, point.y, 0]), 3));

    const dotMaterial = new THREE.PointsMaterial({size: POINT_SIZE, color: 'orange'});
    const dot = new THREE.Points(dotGeometry, dotMaterial);
    scene.add(dot);
  })
}

export function isInRange(number, min, max) {
  return number >= min && number <= max;
}

export function verticalMirrorArray(array, width, height) {
  const temp = [];
  for (let row = height - 1; row >= 0; row--) {
    for (let column = 0; column < width; column++) {
      temp.push(array[column + (width * row)]);
    }
  }
  return temp;
}
