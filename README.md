# 3D World Generator
This project demonstrates a procedural terrain generation technique inspired by Minecraft. By combining multiple layers of multioctave Perlin Noise functions, we create a realistic-looking 3D world with various terrain types and biomes.
![screen1](img/readme/map1.png)
![screen1](img/readme/map2.png)
![screen1](img/readme/map3.png)
![screen1](img/readme/map4.png)
![screen1](img/readme/map5.png)


# Features

### Terrain Types:
Mountains, valleys, hills, cliffs, oceans, lakes, etc.

### Biomes:
Desert, forest, taiga, savanna, etc.

# Getting Started

###  Prerequisites:
Ensure you have `Node.js` installed on your local machine.
Familiarize yourself with procedural terrain generation. I would totally recommend to watch [this video](https://www.youtube.com/watch?v=CSa5O6knuwI) that covers many basic topics.

### Installation:
- Clone this repository.
- Run npm install to install dependencies.
- Run the Project: Execute `npm run start` to launch the development server.
Open your browser and navigate to http://localhost:1234. You can move around the world by using Your mouse. Click `left mouse button` to rotate the camera, `CTRL` to move.

# Customization
You can customize this app by yourself by modifying `js/src/configuration/configuration.js` file. The most important parameters are:
- `GOOD_LOOKING_WORLD`: determines whether the world should look realistic (consumes more resources) or not.
- `CAMERA_CONFIG`: initial camera position.
- `CHUNK_SIZE`: The size of a single terrain chunk.

### Adjusting noise parameters (octaves, persistence, lacunarity)
Terrain shape consists of 3 layers generated using multioctave Perlin Noise functions. Each of them can be configured to achieve different world.

Biomes distribution is also made with Perlin noise and can adjusted as well.

Everything is located in `js/src/noise/noise.js` config file.



## Acknowledgments
Thanks to the Three.js community for their fantastic library.
Inspired by the beauty of procedurally generated worlds in `Minecraft` and `Valheim`.
 ## License
This project is licensed under the MIT License - feel free to explore, modify, and share!


If You have any questions, feel free to ask me on my [LinkedIn profile](https://www.linkedin.com/in/jakub-slowikowski/).
